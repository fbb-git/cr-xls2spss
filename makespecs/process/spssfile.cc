//#define XERR
#include "process.ih"

void Process::spssFile()
{
    FileInfo::Info info{ d_fileInfo("s") };     // info about the spss file

    auto in{ Exception::factory<ifstream>(info.filename) };  // spss csv file

    string optArg;
    unsigned first = 0;                    // idx of 1st var. to process
    if (d_arg.option(&optArg, 's'))
        first = stoul(optArg) - 1;

    Exception::open(d_out, "specs.out", 
                        first == 0 ? ios::out : ios::in | ios::out);

    for (unsigned idx = 1; idx != info.hdr; ++idx)
        in.ignore(10'000, '\n');

    string line;
    getline(in, line);                  // get the header line

    istringstream fields(line);

    unsigned nr = 0;
    string field;

    if (first == 0)
        sheetInfo();
    else
    {
        d_out.seekp(0, ios::end);          // append if first != 0
        
        for (; nr != first; ++nr)       // skip initial variables
            getline(fields, field, ',');
    }

//    loadXLSvars();

    while (getline(fields, field, ','))
    {
        field = field.substr(1, field.length() - 2);    // rm ""
        ++nr;
                                                        // no variable
        if (field.empty() || field.find('.') != string::npos)
            continue;

        if (details(nr, field) == END)
            return;
    }
}   



