//#define XERR
#include "process.ih"

int Process::getType() const
{
    FBB::OneKey key{ OneKey::ON };

    while (true)
    {
        cout << "   enter to stop or var. type (d,i,n,s, or v)? " << flush;
        char ch = key.get();
    
        if ("dinsv"s.find(ch) != string::npos)
        {
            cout.put('\n');
            return ch;
        }

        if (ch == '\n')
            return ch;

        cout << "   *** type `" << ch << "' not supported. Try again\n\n";
    }
}
