//#define XERR
#include "process.ih"

namespace {

char const info[] = R"_(
    ID variable - each sheet MUST have this variable.
    Specify variable numbers of the ID variables in the following sequence 
    of sheets:
  )_";

}

Process::SheetVector Process::getID() const
{
    cout << info;
    for (auto sheet: d_fileInfo.sheets())
        cout << sheet << ' ';
    cout << '\n';

    SheetVector ret;

    do
    {
        cout << "? ";
        string line;
        getline(cin, line);
        if (line.empty())
            break;

        istringstream in(line);

        for (auto sheet: d_fileInfo.sheets())
        {
            unsigned varNr;

            if (not (in >> varNr))
            {
                cout << "   ***specification `" << line << 
                                  "' not enough variables. " <<
                                  d_fileInfo.sheets().size() <<
                                  " required. Try again\n\n  ";
                for (auto sheet: d_fileInfo.sheets())
                    cout << sheet << ' ';
                cout << '\n';

                ret.clear();
                break;
            }

            ret.push_back({ stoul(sheet), varNr });
        }

        if (ret.size() > d_fileInfo.sheets().size())
            ret.resize(d_fileInfo.sheets().size());
    }
    while (ret.empty());

    return ret;
}


