//#define XERR
#include "process.ih"

Process::SheetVector Process::getSheetInfo(int type) const
{
    SheetVector ret;

    if (type == 'i')
        return getID();


    while (true)
    {
        cout << "   XLS sheet and var. number, or plain enter: ";
        string line;
        getline(cin, line);
        if (line.empty())
            break;

        istringstream in(line);
        unsigned sheetNr;
        unsigned varNr;
        if (not (in >> sheetNr >> varNr))
        {
            cout << "   ***specification `" << line << 
                                                "' invalid. Try again\n\n";
            continue;
        }

        ret.push_back({ sheetNr, varNr });
    }    

    return ret;
}
