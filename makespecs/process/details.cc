//#define XERR
#include "process.ih"

Process::Action Process::details(unsigned nr, string const &name)
{
    cout << "\n"
            "\nSPSS variable " << nr << ": " << name << '\n';

    char type = getType();
    if (type == '\n')
        return END;
    
    SheetVector sheetVector{ getSheetInfo(type) };
    
    if (sheetVector.empty())
    {
        cout << "   variable skipped\n";
        return SKIP;                       // at least one
    }

    NMap nMap;

    if (type == 'n')            // n variable: ask transformations
        nMap = getTransformations();

    d_out << name << ' ' << nr << ' ' << type;

    sheets(sheetVector);
    transformations(nMap);
    
    d_out << "\n\n";

    return ACCEPT;                
}
