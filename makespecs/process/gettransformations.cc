//#define XERR
#include "process.ih"

Process::NMap Process::getTransformations() const
{
    NMap ret;

    cout << "   pairs of SPSS values and XLS labels or plain enter:\n";

    while (true)
    {
        string line;
        cout << "   ? ";
        getline(cin, line);
        if (line.empty())
            return ret;

        istringstream in(line);
        string value;
        in >> value;

        string label;
        if (not (getline(in, label)))
        {
            cout << "   ***transformation `" << line << 
                                            "' invalid. Try again\n\n";
            continue;
        }

        ret[value] = String::lc(String::trim(label));
    }    
}
