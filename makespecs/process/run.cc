//#define XERR
#include "process.ih"

void Process::run()
{
    if (d_arg.nArgs() == 1)
        spssFile();
    if (isdigit(d_arg[1][0]))           // xls sheet info requested
        xlsLabels();
    else if (d_arg[1][0] == 's')
        spssLabels();
    else 
        throw Exception{} << "2nd argument must be sheet nr. or 's'";
}
