//#define XERR
#include "process.ih"

void Process::transformations(NMap const &map)
{
    if (map.empty())
        return;

    d_out << " [ ";

    for (
        auto begin = map.begin(), end = map.end(); 
            ++begin != end; 
    )
        d_out << ' ' << begin->first << ": " << begin->second << ',';

    d_out << ' ' << map.begin()->first  << ": " << 
                    map.begin()->second << " ]";
}
