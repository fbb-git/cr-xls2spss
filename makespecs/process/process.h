#ifndef INCLUDED_PROCESS_
#define INCLUDED_PROCESS_

#include <fstream>
#include <string>
#include <vector>
#include <tuple>
#include <unordered_map>

#include <bobcat/arg>

#include "../fileinfo/fileinfo.h"

class Process
{
    enum Action
    {
        END,
        ACCEPT,
        SKIP
    };
    
    typedef std::vector<std::tuple<unsigned, unsigned>> SheetVector;
    typedef std::unordered_map<std::string, std::string> NMap;

    FBB::Arg const &d_arg;
    FileInfo d_fileInfo;
    std::ofstream d_out;            // spss specs

    std::unordered_map<std::string, std::vector<std::string>> d_xlsVars;

    public:
        Process();
        void run();

    private:
        void spssFile();
        void xlsLabels();
        void spssLabels();
        void labelTable(FileInfo::Info info);

        Action details(unsigned nr, std::string const &name);
        int getType() const;

        SheetVector getSheetInfo(int type) const;
        SheetVector getID() const;

        NMap getTransformations() const;

        void sheetInfo();
        void loadXLSvars();
        void sheets(SheetVector const &vect);
        void transformations(NMap const &map);



};
        
#endif
