//#define XERR
#include "process.ih"

void Process::sheetInfo()
{
    for (auto &element: d_fileInfo.map())
    {
        if (isdigit(element.first[0]))
            d_out << "sheet " << element.first << 
                        ": hdr " << element.second.hdr << 
                        ", data " << element.second.data << '\n';
    }
    d_out << '\n';
}
