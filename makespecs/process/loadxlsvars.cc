//#define XERR
#include "process.ih"

void Process::loadXLSvars()
{
    for (auto &element: d_fileInfo.map())
    {
        if (not isdigit(element.first[0]))
            continue;

        auto in{ Exception::factory<ifstream>(element.second.filename) };

        string line;
        for (unsigned idx = 1; idx != element.second.hdr; ++idx)
            getline(in, line);

        istringstream fields(line);
        string field;
        auto &vars = d_xlsVars[element.first];
        while (getline(fields, field, ','))
            vars.push_back(field);
    }
}
