//#define XERR
#include "process.ih"

void Process::labelTable(FileInfo::Info info)
{
                                                // get the xls filename
    auto in{ Exception::factory<ifstream>(info.filename) };

    for (unsigned idx = 1; idx != info.hdr; ++idx)
        in.ignore(10'000, '\n');

    string line;
    getline(in, line);

    istringstream fields(line);

    string optArg;
    unsigned nCols = 3;
    if (d_arg.option(&optArg, 'c'))
        nCols = stoul(optArg);

    TableLines tableLines;
    tableLines << 0;

    for (size_t col = 0, last = nCols * 2 - 1; col != last; ++col)
        tableLines << 2;

    Table table(tableLines, nCols * 2, Table::ROWWISE);

    for (size_t col = 0, last = nCols; col != last; ++col)
        table.setAlign(Align{ 2 * col + 1, left });
    
    string field;
    unsigned nr = 0;

    while (getline(fields, field, ','))
        table << ++nr << field;

    cout << '\n' << info.filename << ", header line at " << 
                    info.hdr << '\n' <<
             table << '\n';
}
