//#define XERR
#include "process.ih"

void Process::sheets(SheetVector const &vect)
{
    for (
        auto begin = vect.begin() + 1, end = vect.end(); 
            begin != end; 
                ++begin
    )
        d_out << ' ' << get<0>(*begin) << ':' << get<1>(*begin) << ',';

    d_out << ' ' << get<0>(vect[0]) << ':' << get<1>(vect[0]);
}
