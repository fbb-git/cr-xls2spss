//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_( [options] info [csv]
Where:
    [options] - optional arguments (short options between parentheses):
        --help (-h)           - provide this help
        --columns (-c) nCols  - number of label name columns (default: 3), 
                                used with xls file processing.
        --spss (-s) nr        - process SPSS variables from variable `nr'
                                when specified information is appended to
                                'specs.out' otherwise `specs.out' is (re)
                                written.
        --version (-v)        - show version information and terminate

    info     - file defining xls files and header/data lines. It must
                contain lines like:
                  -----------------------------------------------------------
                  1 2 3  file     - 1: sheet nr, 2: hdr line, 3: data line,
                                    file: location of the xls file
                  s 2 3 file      - idem, but now `file' is an spss csv file.
                  -----------------------------------------------------------
    csv - info about the csv file to process. 
          For xls files the sheet nr (e.g., 1), for spss: s. 
          These specifications show the variables defined in the specified
          file. 
          If omitted the `specs.out' file is written or updated.
)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}

