//#define XERR
#include "fileinfo.ih"

FileInfo::Info const &FileInfo::operator()(char const *sheet) const
{
    auto iter = d_info.find(sheet);

    if (iter == d_info.end())
        throw Exception{} << "No info about `" << sheet << '\'';

    return iter->second;
}
