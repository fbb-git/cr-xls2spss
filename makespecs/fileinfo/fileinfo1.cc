//#define XERR
#include "fileinfo.ih"

FileInfo::FileInfo(char const *info)
{
    string type;
    Info data;

    auto in{ Exception::factory<ifstream>(info) };

                                            // read the specification elms.
    while (in >> type >> data.hdr >> data.data >> data.filename)
    {
        if (isdigit(type[0]))
            d_sheets.push_back(type);

        d_info[type] = move(data);
    }
}
