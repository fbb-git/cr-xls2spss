#ifndef INCLUDED_FILEINFO_
#define INCLUDED_FILEINFO_

#include <string>
#include <unordered_map>
#include <vector>

struct FileInfo
{
    struct Info
    {
        unsigned hdr;
        unsigned data;
        std::string filename;
    };

    private:
        std::unordered_map<std::string, Info> d_info;
        std::vector<std::string> d_sheets;

    public:
        FileInfo(char const *info);
        Info const &operator()(char const *sheet) const;
        std::unordered_map<std::string, Info> const &map() const;
        std::vector<std::string> const &sheets() const;

    private:
};

inline  std::vector<std::string> const &FileInfo::sheets() const
{
    return d_sheets;
}

inline std::unordered_map<std::string, FileInfo::Info> const &
                                                FileInfo::map() const
{
    return d_info;
}
        
#endif



