//#define XERR
#include "main.ih"

namespace
{
    Arg::LongOption longOpts[] =
    {
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"columns", 'c'},
        Arg::LongOption{"spss", 's'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOpts + size(longOpts);
}

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize("hc:s:v", longOpts, longEnd, argc, argv);
    arg.versionHelp(usage, Icmbuild::version, 1);

    Process process;
    process.run();
}
catch (exception const &ex)
{
    cout << ex.what() << '\n';
    return 1;
}
catch (...)
{
    Arg const &arg = Arg::instance();

    if (arg.option("hv") or arg.nArgs() == 0)
        return 0;

    cout << "unexpected exception\n";
    return 1;
}






