#include "main.ih"

namespace   // the anonymous namespace can be used here
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption{"verbose", 'V'},          // log debug messages to cout
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"output", 'o'},
        Arg::LongOption{"type", 't'},
        Arg::LongOption{"version", 'v'},
        Arg::LongOption{"warnings", 'w'},
    };
    auto longEnd = longOptions + size(longOptions);

    filebuf s_fb;
}

ofstream s_out;

bool g_log;

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize(
                        "ho:tvVw", 
                        longOptions, longEnd,
                        argc, argv
                     );
    arg.versionHelp(usage, Icmbuild::version, 4);

    options(&s_fb, arg);           // handle -o and -V

    Parser parser;
    parser.parse();

    if (parser.errors())
        return 1;

    parser.inspect();

    SPSSData spssData{ parser };            // read in the SPSS csv file

    XLSData xlsData{ parser };              // and the Excel csv file

    spssData.merge(xlsData);

    parser.writeSpecs();                    // write the new specs file

    if (spssData.errors())
        return 1;

    spssData.writeCSV();                    // write the new data csv file
}
catch (exception const &exc)
{
    cout << exc.what() << '\n';
    return 1;
}
catch (int err)
{
    return Arg::instance().option("hv") ? 0 : err;
}
catch (...)
{
    cout << "unexpected end of program\n";
    return 1;
}
