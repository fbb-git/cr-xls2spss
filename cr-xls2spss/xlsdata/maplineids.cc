#include "xlsdata.ih"

void XLSData::mapLineIDs()   
{
    unsigned idIdx = d_parser.xlsIDidx();
    string const &idName = d_parser.idName();

    for (
        unsigned dataIdx = d_fstRowIdx, end = data().size(); 
            dataIdx != end; 
                ++dataIdx
    )
    {
        string const &idValue = data()[dataIdx][idIdx];    // idValue in line 
                                                            // 'dataIdx'

        auto iter = d_ID2line.find(idValue);

        if (iter != d_ID2line.end())          // already present: corrupted
            throw Exception{} << "XLS data file `" << d_csvName << 
                    "': saw ID " << idName <<
                    " (col. " << idIdx + 1 << 
                    ") multiple times. Data line " << iter->second << 
                    " and data line " << dataIdx;

        d_ID2line[idValue] = dataIdx;              // map idValue -> lineIdx
    }
}




