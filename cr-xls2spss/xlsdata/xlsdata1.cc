#include "xlsdata.ih"

//  0       1       2       3
// specs spss.csv xls.csv sheetNr

XLSData::XLSData(Parser const &parser)
:
    CSV4180( 0, true ),         // header, CSV4180 determines #fields
    d_arg(Arg::instance()),
    d_parser(parser),
    d_csvName(d_arg[2]),
    d_data0nr(get<1>(parser.sheetInfo())),
    d_idIdx(parser.xlsIDidx())
{
    unsigned hdr = get<0>(parser.sheetInfo());  // find hdr and data row nrs.
    d_fstRowIdx = d_data0nr - hdr - 1;

    ifstream csv{ Exception::factory<ifstream>(d_csvName) };

    for (unsigned idx = 0; idx != hdr; ++idx)
        csv.ignore(10000, '\n');            // ignore lines until the header

    read(csv);

    if (g_log)
        cout << "#CSV fields in " << d_csvName << ": " << nValues() << '\n';

    mapLineIDs();
}

