#ifndef INCLUDED_XLSDATA_
#define INCLUDED_XLSDATA_

#include <string>
#include <unordered_map>
#include <vector>
#include <tuple>

#include <bobcat/arg>
#include <bobcat/csv4180>

#include "../types/types.h"

class Parser;

class XLSData: private FBB::CSV4180, private Types
{
//    typedef std::vector<unsigned>                       LineIdxVector;
//    typedef std::unordered_map<std::string, LineIdxVector> 
//                                                        StrLineIdxVectorMap;

    FBB::Arg const &d_arg;
    Parser const &d_parser;
    std::string d_csvName;

    StrUMap d_ID2line;                          // id(string) -> data line

//    StrLineIdxVectorMap d_var2col;

    unsigned d_fstRowIdx;
    unsigned d_data0nr;                         // CSV row of 1st data line
    unsigned d_idIdx;                           // col idx of the ID var.

    public:
        XLSData(Parser const &parser);
                                                // returns data row idx
        StrVect const &operator[](unsigned idx) const;  

        StrUMap const &id2line() const;         // map of id(str) -> data line

        unsigned data0nr() const;              // nr of first CSV data line

        unsigned idIdx() const;                     // col. idx of the ID var.

//        bool find(unsigned *xlsIdx, std::string const &spssVar) const;
//        unsigned firstRowIdx() const;          // first line index to use

    private:
//        bool headerLabels() const;

        void mapLineIDs();
};

inline XLSData::StrVect const &XLSData::operator[](unsigned idx) const
{
    return data()[idx];
}

inline XLSData::StrUMap const &XLSData::id2line() const
{
    return d_ID2line;
}

inline unsigned XLSData::data0nr() const
{
    return d_data0nr;
}

inline unsigned XLSData::idIdx() const
{
    return d_idIdx;
}

//inline unsigned XLSData::firstRowIdx() const
//{
//    return d_fstRowIdx;
//}
//
        
#endif





