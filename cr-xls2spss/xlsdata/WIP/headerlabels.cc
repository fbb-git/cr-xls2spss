#include "xlsdata.ih"

bool XLSData::headerLabels() const
{
    string type;
    if (not d_arg.option(&type, 'H'))
        return false;

    if (type[0] == 'x')
    {
        for (auto const &label: header())
            cout << label << '\n';
    }

    return true;
}
