#include "xlsdata.ih"

void XLSData::var2col()
{
    auto const &names = header();           // vector of var. names

                                            // for each variable: associate
                                            // name -> column
    for (unsigned colIdx = 0, end = nValues(); colIdx != end; ++colIdx)
        d_var2col[ names[colIdx] ].push_back( colIdx );

    auto iter = d_var2col.find(d_parser.idName());
    if (iter == d_var2col.end())
        throw Exception{} << "XLS data file `" << d_csvName << 
                            ": no ID variable `" << d_parser.idName() <<'\'';

    d_idIdx = iter->second[0];
}
