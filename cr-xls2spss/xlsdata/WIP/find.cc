#include "xlsdata.ih"

//        struct Parser::XLSInfo
//        {
//            std::string const &xlsVar;
//            unsigned idx = 0;           // index in the SourceSpec vector
//        };

bool XLSData::find(unsigned *xlsIdx, string const &spssVar) const
{
    Parser::XLSInfo info = d_parser.xlsInfo(spssVar);

    auto iter = d_var2col.find(info.xlsVar);

    if (iter == d_var2col.end())
        return false;

    *xlsIdx = iter->second[info.idx];       // set the XLS variable index
    return true;
}
