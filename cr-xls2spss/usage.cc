//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_(" [options] specs spss.csv xls.csv sheetNr
Where:
    [options] - optional arguments (short options between parentheses):
        --help (-h)         - provide this help
        --output (-o) path  - output is sent to `path' instead of cout
        --type (-t) type    - data type to use if not specified 
                              ([idns], default n)
        --verbose (-V)      - show verbose information about the program's
                              actions 
        --version (-v)      - show version information and terminate
        --warnings (-w)     - mismatches between SPSS data fields en XLS data
                              fields result in warnings, leaving the SPSS data
                              as-is.
    
    specs    - name of the file containing the merge-specifications,
    spss.csv - name of the spss csv data file,
    xls.csv  - name of the excel csv data file,
    sheetNr  - number of the excel sheet.

)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}
