#include "main.ih"

namespace   // the anonymous namespace can be used here
{
    Arg::LongOption longOptions[] =
    {
        Arg::LongOption{"help", 'h'},
        Arg::LongOption{"keep-dir", 'k'},
        Arg::LongOption{"version", 'v'},
    };
    auto longEnd = longOptions + size(longOptions);
}

SyslogStream g_syslog{ "cr-mailhandler" };

int main(int argc, char **argv)
try
{
    Arg const &arg = Arg::initialize(
                        "hkv", 
                        longOptions, longEnd,
                        argc, argv
                     );
    arg.versionHelp(usage, Icmbuild::version, 0);

    g_syslog << "starting" << endl;

    TmpDir tmpDir{ "/tmp/cr-" };        // removes the tmp dir at exit

    MailHandler mh;

    mh.write("specs");
    mh.write("spss");
    mh.write("xls");
    g_syslog << "files received" << endl;

    mh.xml2spss();
    g_syslog << "cr-xms2spss completed" << endl;

    mh.collect();
    g_syslog << "cr.zip constructed" << endl;

    mh.sendmail();    
    g_syslog << "mail sent to " << mh.replyTo() << endl;
}
catch (exception const &exc)
{
    g_syslog << "exception: " << exc.what() << endl;
    return 0;
}
catch (...)
{
    g_syslog << "unexpected exception" << endl;
    return 0;
}



