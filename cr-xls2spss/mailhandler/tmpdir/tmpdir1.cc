#include "tmpdir.ih"

TmpDir::TmpDir(char const *basePath)
{
    pid_t pid = getpid();

    for (unsigned attempt = 0; attempt != 10; ++attempt)
    {
        d_dirName = basePath + to_string(pid);

        if (fs::create_directory(d_dirName))
        {
            fs::current_path(d_dirName);
            return;
        }

        ++pid;                              // try the next nr 
    }

    throw Exception{} << "No " << basePath << "... directory available";
}
