//                     usage.cc

#include "main.ih"

namespace {
char const info[] = R"_(" [options] specs spss.csv xls.csv sheetNr
Where:
    [options] - optional arguments (short options between parentheses):
        --help (-h)         - provide this help
        --keep-dir (-k)     - keep the /tmp/cr-... directory after completion
                              of the program
        --version (-v)      - show version information and terminate

)_";

}

void usage(std::string const &progname)
{
    cout << "\n" <<
    progname << " by " << Icmbuild::author << "\n" <<
    progname << " V" << Icmbuild::version << " " << Icmbuild::years << "\n"
    "\n"
    "Usage: " << progname << info;
}
