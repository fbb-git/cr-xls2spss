#include "mailhandler.ih"

void MailHandler::write(char const *fname) const
{
    string collect;
    bool base64 = section(&collect);        // skip the mail text
    out(fname, collect, base64);
}
