#include "mailhandler.ih"

bool MailHandler::section(string *collect) const
{
    string line;

    bool base64 = false;

    while (getline(cin, line))                      // find base64
    {
        if (line.find("Content-Transfer-Encoding: base64") == 0)
        {
            base64 = true;
            continue;
        }

        if (line.empty())                           // find the empty line
            break;
    }

    collect->clear();

    while (getline(cin, line))
    {
        if (line.find(d_boundary) == 0)
            break;

        (*collect += line) += '\n';
    }

    if (not cin)
        throw Exception{} << "Premature end of file";

    return base64;
}



