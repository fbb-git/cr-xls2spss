#include "mailhandler.ih"

void MailHandler::execute(string const &cmd) const
{
    Process process{ Process::NONE, cmd };

    process.start();

    int exitVal = process.waitForChild();

    if (exitVal != 0)
        throw Exception{} << cmd.substr(0, cmd.find(' ')) << 
                             " process error (" << exitVal << ')';
}
