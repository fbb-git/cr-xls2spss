#include "mailhandler.ih"


// (
//     (
//         <([^>]+)>               as in: this <addr@dom.nl> and more
//     )                                       <  [^>]+    >
//     |
//     (
//         \s([^<@ ]+@[^ >]+)    as in:     username@domain.nl   
//     )                                  \s[^<@ ]+ @  [^ >]+
// )
// end() == 4: matches the 1st part, [3] is the address
// end() == 6: matches the 2nd part, [5] is the address.

Pattern MailHandler::s_mailAddress
                     { 
                                R"(((<([^>]+)>)|(\s([^<@ ]+@[^ >]+))))" 
                     };

initializer_list<char const *> MailHandler::s_replyHdrs
            {
                "Return-Path",
                "Reply-To",
                "From"
            };


