#include "mailhandler.ih"

void MailHandler::skipMail() const
{
    string line;                                  
    while (getline(cin, line) && line != d_boundary)    // find the 1st bdry
        ;

    string collect;
    section(&collect);
}
