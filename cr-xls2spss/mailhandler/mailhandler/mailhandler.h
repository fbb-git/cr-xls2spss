#ifndef INCLUDED_MAILHANDLER_
#define INCLUDED_MAILHANDLER_

#include <initializer_list>

#include <string>
#include <bobcat/pattern>

namespace FBB
{
    class MailHeaders;
}

class MailHandler
{
    std::string d_boundary;
    std::string d_replyTo;
    std::string d_sheet;

    static FBB::Pattern s_mailAddress;
    static std::initializer_list<char const *> s_replyHdrs;

    public:
        MailHandler();

        void write(char const *fname) const;
        void xml2spss() const;
        void collect() const;
        void sendmail() const;

        std::string const &replyTo() const;

    private:
        void getReplyAddress(FBB::MailHeaders &mh);

        void skipMail() const;
        bool section(std::string *collect) const;
        void out(char const *fname, std::string const &collect, 
                                                    bool base64) const;

        void execute(std::string const &cmd) const;

};

inline std::string const &MailHandler::replyTo() const
{
    return d_replyTo;
}
        
#endif




