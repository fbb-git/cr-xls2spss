#include "mailhandler.ih"

// echo "This is the message body" | mutt -a "/path/to/file.to.attach" -s
// "subject of message" -- recipient@domain.com

void MailHandler::sendmail() const
{
    execute(
        R"(/usr/bin/mutt -s "cr-xml2spss output" -a cr.zip -- )" +
        d_replyTo
    );
}
