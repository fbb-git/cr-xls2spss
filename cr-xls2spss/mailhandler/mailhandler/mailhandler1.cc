#include "mailhandler.ih"

MailHandler::MailHandler()
{
    MailHeaders mh{ cin, MailHeaders::READ };

    getReplyAddress(mh);

    mh.setHeaderIterator("Content-Type");
    auto iter = mh.beginh();

    d_boundary = iter->substr( iter->find('"') + 1);    // skip the 1st "
    d_boundary.pop_back();
    d_boundary.insert(0, "--");
    //cout << "Boundary: `" << d_boundary << "'\n";

    mh.setHeaderIterator("Subject");
    iter = mh.beginh();
    if (iter == mh.endh())
        throw Exception{} << "No Subject: header found";

    d_sheet = iter->substr(
                    iter->find_first_of("123456789")    // get the sheet nr.
                );
    
    skipMail();                                 // skip the mail content
}
