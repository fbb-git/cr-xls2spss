#include "mailhandler.ih"

void MailHandler::out(char const *fname, string const &collect, 
                                                    bool base64) const
{
    ofstream dest{ Exception::factory<ofstream>(fname)};

    if (not base64)
        dest << collect;
    else
    {
        istringstream in64{ collect };
        IBase64Stream<DECRYPT> in{ in64 };
        dest << in.rdbuf();
    }
}
