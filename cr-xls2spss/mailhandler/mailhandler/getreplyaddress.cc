#include "mailhandler.ih"

void MailHandler::getReplyAddress(MailHeaders &mh)
{
    for (char const *header: s_replyHdrs)
    {
        mh.setHeaderIterator(header);
        auto iter = mh.beginh();
        if (iter != mh.endh())
        {
            //cout << *iter << '\n';
            s_mailAddress.match(*iter);

            d_replyTo = s_mailAddress[
                        s_mailAddress.end() == 6 ? 5 : 3    // see data.cc
                    ];   
    
            //cout << "reply to: `" << d_replyTo << "'\n";
            return;
        }
    }

    throw Exception{} << "No `Reply-To:' or `From:' found";
}

