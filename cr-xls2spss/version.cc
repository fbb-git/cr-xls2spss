//                     version.cc

#include "main.ih"
#include "icmconf"

#include "VERSION"

namespace Icmbuild
{
    char version[]  = VERSION;
    char years[]     = YEARS;
    
    char author[] = AUTHOR;
}
