#include "spssdata.ih"

//  0       1       2       3
// specs spss.csv xls.csv sheetNr

SPSSData::SPSSData(Parser &parser)
:
    CSV4180( 0, true ),         // header, CSV4180 determines #fields
    d_arg(Arg::instance()),
    d_csvName(d_arg[1]),
    d_parser(parser)
{
    ifstream csv{ Exception::factory<ifstream>(d_csvName) };
    read(csv);

    d_data = std::move(release());      // local storage of the CSV data

    if (g_log)
        cout << "#CSV fields in " << d_csvName << ": " << nValues() << '\n';

    d_warnings = d_arg.option('w');

    specialTypes();                     // collect s, n, d types
    mapLineIDs();                       // find data line given ID
}



