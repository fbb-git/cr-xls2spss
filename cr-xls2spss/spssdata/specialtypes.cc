#include "spssdata.ih"

void SPSSData::specialTypes()
{
                                            // for each variable: associate
                                            // name -> column
    for (unsigned colIdx = 0, end = nValues(); colIdx != end; ++colIdx)
    {
        switch (d_parser.spssVarType(colIdx))
        {
            case 'd':
                d_dVars.insert(colIdx);
            break;

            case 'n':
                d_nVars.insert(colIdx);
            break;

            case 's':
                d_sVars.insert(colIdx);     // collect var indices of s-type
            break;                          // variables
        }
    }       
}

