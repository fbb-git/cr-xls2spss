#include "spssdata.ih"

void SPSSData::mapLineIDs()   
{
    auto &[ idName, idIdx] = d_parser.id();     // get the spss ID column

    xerr(idName << " at idx " << idIdx << ", col: " << (idIdx + 1));

                                                // visit all data lines
    for (size_t lineIdx = 0, end = d_data.size(); lineIdx != end; ++lineIdx)
    {
                                                // ID value in line 'lineIdx'
        string const &idNr = d_data[lineIdx][idIdx]; 

        auto iter = d_ID2line.find(idNr);        // check whether the ID is
                                                // unique 
        if (iter != d_ID2line.end())             // already present: corrupted
        {
            cout << "SPSS data: saw ID " << idName <<
                    " (col. " << (idIdx + 1) << ", nr. " << idNr <<
                    ") multiple times. Line " << (iter->second + 1) << 
                    " and line " << (lineIdx + 1) << '\n';
            d_errors = true;
        }

        d_ID2line[idNr] = lineIdx;              // map idNr -> lineIdx
    }
}
