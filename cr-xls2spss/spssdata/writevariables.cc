#include "spssdata.ih"

    // write the names of all variables and their types
    // E.g., ID F T0Group F T0Groep2 F
void SPSSData::writeVariables(ostream &out) const
{
    auto const &var = header();
    auto const &varInfoMap = d_parser.varInfoMap();

    for (
        unsigned idx = 0, end = var.size();
            idx != end; 
                )
    {
        string const &spssName = var[idx];
        auto const &iter = varInfoMap.find(idx + 1);    // variable nr. needed

        char const *type = iter == varInfoMap.end() ? "F" : 
                                                   iter->second.SPSStype();
        out << spssName << ' ' << type << 
                ((++idx % 3 == 0) ? "\n    " : " ");
    }
    
    out << ".\n";
}
