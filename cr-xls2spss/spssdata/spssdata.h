#ifndef INCLUDED_SPSSDATA_
#define INCLUDED_SPSSDATA_

#include <iosfwd>
#include <unordered_set>

#include <bobcat/arg>
#include <bobcat/csv4180>

#include "../main.h"
#include "../parser/parser.h"
#include "../types/types.h"

class XLSData;

class SPSSData: private FBB::CSV4180, private Types
{
    typedef std::vector<StrVect>                        DataMatrix;
    typedef std::unordered_set<unsigned>                USet;

    FBB::Arg const &d_arg;
    std::string d_csvName;
    Parser &d_parser;

    StrUMap  d_ID2line;             // given ID nr (from the CSV, so a
                                    // string),  which line idx to use?
    DataMatrix d_data;

    USet d_dVars;
    USet d_sVars;
    USet d_nVars;               // convert labels to numbers

    unsigned d_errors = 0;
    bool d_warnings = false;

    static size_t const s_spssBase = 12'219'379'200;

    public:
        SPSSData(Parser &parser);

        void merge(XLSData const &xlsData);
        void writeCSV() const;

        bool errors() const;

    private:
        void specialTypes();
        void mapLineIDs();

                                        // tuple: <0>: idIdx, <1>: rowNr
        void mergeXLS(std::string const &xlsID, StrVect const &xlsData,
                                                UUTuple const &xlsInfo);

        void mergeRow(unsigned spssRowIdx, StrVect const &xlsData,
                                           unsigned xlsRowNr);

        void append(unsigned idIdx, StrVect const &xlsData);

        void mismatch(
                std::string const &spssEl, unsigned spssRowIdx, 
                unsigned spssIdx, bool isDate, 
                std::string const &xlsEl, StrVect const &xlsData,
                unsigned xlsIdx, unsigned xlsRowNr
            );

        void insertInto(std::ostream &&out) const;
        void insertHeader(std::ostream &out) const;
        void insert(std::ostream &out, StrVect const &vect) const;

        void writeGetData() const;
        void writeVariables(std::ostream &out) const;


        bool sVar(unsigned idx) const;                              // .ih
        bool dVar(unsigned idx) const;                              // .ih
        bool nVar(unsigned idx) const;                              // .ih

        void assignNvar(std::string const &xlsEl);

        static std::string spssDate(std::string const &xlsData);
        static std::string setDate(std::string const &spssSec);
        static bool empty(std::string const &str);                  // .ih
};

inline bool SPSSData::errors() const
{
    return d_errors != 0;
}

#endif

