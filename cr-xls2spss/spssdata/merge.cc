#include "spssdata.ih"

void SPSSData::merge(XLSData const &xlsData)
{
                                        // merge all xls lines with the spss
                                        // data
    unsigned idIdx = xlsData.idIdx();
    unsigned data0nr = xlsData.data0nr();

    for (auto &[xlsID, xlsRowIdx]: xlsData.id2line())
        mergeXLS(xlsID, xlsData[xlsRowIdx], 
                 UUTuple{ idIdx, xlsRowIdx + data0nr });
}
