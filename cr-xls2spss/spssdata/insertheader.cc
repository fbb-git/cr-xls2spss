#include "spssdata.ih"

void SPSSData::insertHeader(ostream &out) const
{
    auto const &label = header();

    for (unsigned idx = 0, last = nValues() - 1; ; ++idx)
    {
        out << '"' << label[idx] << '"';

        if (idx == last)
        {
            out.put('\n');
            return;
        }

        out.put(',');
    }
}
