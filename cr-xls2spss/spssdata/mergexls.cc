#include "spssdata.ih"

    // visit all xls data rows. If a matching spss row exists, then 
    // merge the xls data into the spss row. If no matching spss row exists
    // then append the xls data row 
    
    // xlsData: the data corresponding to xlsID
    // xlsInfo: <0>: idIdx, <1>: xlsRowNr
void SPSSData::mergeXLS(string const &xlsID, StrVect const &xlsData,
                                             UUTuple const &xlsInfo)
{
    if (                                // find the matching SPSS data line
        auto iter = d_ID2line.find(xlsID); 
        iter != d_ID2line.end()
    )
                    // second: spss line idx,   merge with existing spss data
        mergeRow(iter->second, xlsData, get<1>(xlsInfo));  // <1>: xls row nr.
    else
        append(get<0>(xlsInfo), xlsData);       // or append a new line
}
