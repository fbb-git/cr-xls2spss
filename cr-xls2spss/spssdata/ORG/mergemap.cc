#include "spssdata.ih"

SPSSData::UUTupleVector SPSSData::mergeMap(XLSData const &xlsData)
{
    auto const &var = header();

    d_parser.onlyThisSheet();               // SourceSpec info is reduced to
                                            // info about only the current
                                            // sheet


    UUTupleVector ret;                      // SPSS var index -> XLS var index

        // visit all SPSS variables
    for (unsigned idx = 1, end = header().size(); idx != end; ++idx)
    {
        string const &spssVar = var[idx];   // this SPSS variable

        unsigned xlsIdx;
        if (xlsData.find(&xlsIdx, spssVar)) // find the matching XLS variable
        {
            if (g_log)
            {
                Parser::XLSInfo xlsInfo = d_parser.xlsInfo(spssVar);

                cout << "SPSS " << spssVar << ':' << idx << " = "
                        "XLS " << xlsInfo.xlsVar << ':' << xlsIdx << '\n';
            }

            ret.push_back({ idx, xlsIdx });
        }
    }

    return ret;
}
