#include "spssdata.ih"

void SPSSData::mismatch(string const &spssEl, unsigned spssRowIdx, 
                        unsigned spssIdx, bool isDate, 
                        
                        string const &xlsEl, StrVect const &xlsData, 
                        unsigned xlsIdx, unsigned xlsRowNr
                    )
{
    StrVect const &spssRow = d_data[spssRowIdx];

    string spssDate;
    string xlsDate;

    if (isDate)
    {
        spssDate = setDate(spssEl);
        xlsDate = setDate(xlsEl);
    }

    if (d_warnings)
        cout << "\n[warning] ";
    else
        cout << "\n[error " << ++d_errors << "] ";

    cout << "difference:\n"
        "    SPSS[" << (spssRowIdx + 2) <<          // 1 header + idx -> nr
            ", " <<  (spssIdx + 1) << "] (" << spssEl << spssDate << "):\n"
        "    XLS[" << xlsRowNr << ", " <<  (xlsIdx + 1) << 
                    "] (" << xlsEl << xlsDate;

    if (xlsEl != xlsData[xlsIdx])
        cout << ", orig: " << xlsData[xlsIdx];

    cout << ")\n"
            "SPSS line: ";
    for (size_t idx = 0; idx <= spssIdx; ++idx)
        cout << spssRow[idx] << ',';
    cout << '\n';

    cout << "XLS line: ";
    for (size_t idx = 0; idx <= xlsIdx; ++idx)
        cout << xlsData[idx] << ',';
    cout << '\n';
}
