#include "spssdata.ih"

void SPSSData::writeCSV() const
{
    insertInto(Exception::factory<ofstream>(d_csvName + ".new"));
    writeGetData();
}
