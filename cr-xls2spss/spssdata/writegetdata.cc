#include "spssdata.ih"

void SPSSData::writeGetData() const
{
    ofstream out{ Exception::factory<ofstream>(d_csvName + ".syntax") };

    out <<                                      //  using defaults for:
        "GET DATA  /TYPE = TXT\n"
                                                //  /ENCODING='Locale'
        "  /FILE = '" << d_csvName << "'\n"
                                                //  /DELCASE = LINE
        "  /DELIMITERS = \",\"\n"
                                                //  /ARRANGEMENT = DELIMITED
        "  /FIRSTCASE = 2\n"  // skip hdr line                  
        "  /VARIABLES = ";

    writeVariables(out);
}
