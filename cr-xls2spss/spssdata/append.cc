#include "spssdata.ih"

void SPSSData::append(unsigned idIdx, StrVect const &xlsData)
{
    StrVect dest(nValues());                  // new SPSS data line

    cout << "new ID " << xlsData[idIdx] << '\n';

    for (auto &[spssIdx, xlsIdx]: d_parser.spss2xls())
    {
        string xlsEl = xlsData[xlsIdx];         // get the XLS value (local
                                                // copy needed below)

        if (nVar(spssIdx))                          // n-variable?
            xlsEl = d_parser.nVar(spssIdx, xlsEl);  // convert label to value
        else if (dVar(spssIdx))                     // Date-variable?
            xlsEl = spssDate(xlsEl);                // xls date to spss data
        
        dest[spssIdx] = xlsEl;
    }

    d_data.push_back(move(dest));
}
