#include "spssdata.ih"

string SPSSData::setDate(string const &spssSec)
{
    DateTime dt{ static_cast<time_t>(stoull(spssSec) - s_spssBase), 
                DateTime::UTC };

    ostringstream out;
    out << " (= " << dt.monthDayNr() << '-' << (dt.month() + 1) << '-' << 
                                                     dt.year() << ')';
    return out.str();
}
