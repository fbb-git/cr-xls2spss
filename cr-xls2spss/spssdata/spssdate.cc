#include "spssdata.ih"

//static
string SPSSData::spssDate(string const &xlsDate)
{
    static time_t const excelBase = 2'209'161'600;

    time_t sec;

    if (xlsDate.find('.') == string::npos)      // no date-specification
        sec = stoull(xlsDate) * 24 * 3600       // containing dots
                - excelBase + s_spssBase;
    else
    {
        DateTime dt;

        dt.setHours(0);
        dt.setMinutes(0);
        dt.setSeconds(0);

        istringstream is(xlsDate);
        char dot;
        unsigned day;
        unsigned mon;
        unsigned year;

        if (not (is >> day >> dot >> mon >> dot >> year))
        {
            year = mon % 100;
            mon /= 100;
        }
                
        dt.setYear(2000 + year);
        dt.setMonth(mon - 1);
        dt.setDay(day);

        sec = dt.utcSeconds() + s_spssBase;
    }

    return to_string(sec);
}
