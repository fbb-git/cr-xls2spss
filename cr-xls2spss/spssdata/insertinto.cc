#include "spssdata.ih"

void SPSSData::insertInto(ostream &&out) const
{
    insertHeader(out);

    for (StrVect const &vect: d_data)
        insert(out, vect);
}
