#include "spssdata.ih"


void SPSSData::mergeRow(unsigned spssRowIdx, StrVect const &xlsData,
                                             unsigned xlsRowNr)
{
    StrVect &spssData = d_data[spssRowIdx];

    for (auto &[spssIdx, xlsIdx]: d_parser.spss2xls())
    {
        string xlsEl = xlsData[xlsIdx];         // get the XLS value (local
                                                // copy needed below)

        if (empty(xlsEl))                       // no matching XLS value
            continue;

        bool isDate = dVar(spssIdx);

        if (nVar(spssIdx))                          // n-variable?
            xlsEl = d_parser.nVar(spssIdx, xlsEl);  // convert label to value
        else if (isDate)                            // Date-variable?
            xlsEl = spssDate(xlsEl);                // xls date to spss data

        string &spssEl = spssData[spssIdx];     // the SPSS value may directly
                                                // be modified in the vector
        if (empty(spssEl))
            spssEl = xlsEl;                     // assign if 1st encountered
        else if (spssEl != xlsEl)               // or equality check
            mismatch(spssEl, spssRowIdx, spssIdx, isDate, 
                     xlsEl, xlsData, xlsIdx, xlsRowNr);
    }
}

