#include "spssdata.ih"

void SPSSData::insert(ostream &out, StrVect const &vect) const
{
    for (unsigned idx = 0, last = nValues() - 1; ; ++idx)
    {
        char const *sep = sVar(idx) ? "\"" : "";  // s-type "-separator

        out << sep << vect[idx] << sep;                 // insert the word

        if (idx == last)                                // the last word:
        {                                               // eoln and done
            out.put('\n');
            return;
        }

        out.put(',');                                   // or comma-separated
    }
}
