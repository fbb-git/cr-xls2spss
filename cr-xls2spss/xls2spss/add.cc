//#define XERR
#include "xls2spss.ih"

void XLS2SPSS::add(unsigned idx, std::string const &name, 
                   UUTupleVector const &ss)
{
    for (auto &[sheetNr, xlsIdx]: ss)
        d_useMap[sheetNr][xlsIdx + 1].push_back({ name, idx + 1 });
}
