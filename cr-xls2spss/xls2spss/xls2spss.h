#ifndef INCLUDED_XLS2SPSS_
#define INCLUDED_XLS2SPSS_

#include "../types/types.h"

class XLS2SPSS: private Types
{
    std::unordered_map<
        unsigned,                       // sheet
        std::unordered_map<
            unsigned,                   // xls var. used by:
            std::vector<SNTuple>         // spss variable and nr
        >
    >   d_useMap;

    public:
        XLS2SPSS();
        void add(unsigned idx, std::string const &name, 
                 UUTupleVector const &ss);

        void inspect();                 // can only be used once.
};
        
#endif
