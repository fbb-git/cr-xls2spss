//#define XERR
#include "xls2spss.ih"

void XLS2SPSS::inspect()
{
    for (auto const &sheet: d_useMap)
    {
        for (auto const &xlsVar: sheet.second)
        {
            if (xlsVar.second.size() > 1)
            {
                cout << "sheet " << sheet.first << 
                        ", xls var. " << xlsVar.first << " is used for ";

                for (auto const &[name, nr]: xlsVar.second)
                    cout << name << " (" << nr << "), ";
    
                cout << '\n';
            }
        }
    }

    d_useMap.clear();
}
