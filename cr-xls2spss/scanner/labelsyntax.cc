#include "scanner.ih"

void Scanner::labelSyntax() const
{
    throw Exception{} << "line " << lineNr() << ": label `" << matched() <<
                        "' ends in newline";
}
