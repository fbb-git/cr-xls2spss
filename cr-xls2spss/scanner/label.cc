#include "scanner.ih"

int Scanner::label()
{
    begin(StartCondition_::INITIAL);

    string tmp{ matched() };

    push(tmp.back());

    tmp.pop_back();
    
    setMatched(String::trim(tmp));

    return Parser::LABEL;
}
