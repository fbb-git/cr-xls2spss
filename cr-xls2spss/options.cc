#include "main.ih"

void options(filebuf *fb, Arg const &arg)
{
    g_log = arg.option('V');

    string value;
    if (arg.option(&value, 'o'))
    {
        if (not (fb->open(value.c_str(), ios::out)->is_open()))
            throw Exception{} << "can't write `" << value << '\'';

        cout.rdbuf(fb);
    }
}
