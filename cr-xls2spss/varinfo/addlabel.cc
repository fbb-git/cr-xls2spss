#include "varinfo.ih"

void VarInfo::addLabel(string const &label, string const &value)
{
    d_am[String::lc(String::trim(label))] = value;
}
