#include "varinfo.ih"

void VarInfo::write(ostream &out) const
{
    out << static_cast<char>(d_type) << ' ';

    writeSheetInfo(out);//    SourceSpec::write(out);

    if (not d_am.empty())
    {
        out << " [ ";

        for (auto begin = d_am.begin(), end = d_am.end();
                ++begin != end;
                    )
            out << begin->second << ": " << begin->first << ", ";

        out << d_am.begin()->second << ": " << d_am.begin()->first;

        out << " ]";
    }

    out << '\n';
}
