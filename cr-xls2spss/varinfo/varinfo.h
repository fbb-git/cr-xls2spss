#ifndef INCLUDED_VARINFO_
#define INCLUDED_VARINFO_

#include <string>
#include <ostream>

#include "../types/types.h"

class VarInfo: private Types
{
    std::string d_varName;          // spss variable name

    unsigned d_lineNr;              // line nr in the specs file

    unsigned d_spssIdx = ~0U;       // col index in of the spss var. in data()
                                    // or header()

    int d_type = 'v';               // dinsv type (default: number value)

    UUTupleVector d_sheetInfo;      // sheet NRS. and variable INDICES of 
                                    // matching XLS variables

    AssociationMap d_am;            // used with type 'n' to store 
                                    // label-number associations

                                    // map 'dinsv' to SPSS GET DATA types
    static std::unordered_map<int, char const *> s_type;

    public:
        VarInfo() = default;
        VarInfo(std::string const &varName,                         // 1
                size_t lineNr, int type, UUTupleVector &ss,      
                AssociationMap &am);   

        unsigned xlsIdx(unsigned sheetNr) const;    // return the XLS var. idx
                                                    // matching the SPSS var
                                                    // in sheet `sheetNr'
                                                    // ~0U if none

        int setColIdx(unsigned colIdx);     // returns d_type

        unsigned spssIdx() const;
        unsigned specsLineNr()  const;
        int type() const;
        char const *SPSStype() const;
        std::string const &name() const;

        AMconstIterator findLabel(std::string const &label) const;
        AMconstIterator end() const;
        
        AssociationMap const &labelMap() const;
        void addLabel(std::string const &label, std::string const &value);

        void write(std::ostream &out) const;
        void writeSheetInfo(std::ostream &out) const;
};

inline std::string const &VarInfo::name() const
{
    return d_varName;
}

inline VarInfo::AMconstIterator VarInfo::end() const
{
    return d_am.end();
}

inline VarInfo::AssociationMap const &VarInfo::labelMap() const
{
    return d_am;
}

inline unsigned VarInfo::spssIdx() const
{
    return d_spssIdx;
}

inline unsigned VarInfo::specsLineNr() const
{
    return d_lineNr;
}

inline int VarInfo::type() const
{
    return d_type;
}

inline char const *VarInfo::SPSStype() const
{
    return s_type.find(d_type)->second;
}

#endif
