#include "varinfo.ih"

VarInfo::AMconstIterator VarInfo::findLabel(string const &label) const
{
    return d_am.find(String::lc(label));
}
