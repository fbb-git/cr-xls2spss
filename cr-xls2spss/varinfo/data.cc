#include "varinfo.ih"

unordered_map<int, char const *> VarInfo::s_type = 
{
    { 'd',  "ADATE8" },
    { 'i',  "F" },
    { 'n',  "F" },
    { 's',  "A" },
    { 'v',  "F" },
};
