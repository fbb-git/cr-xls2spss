#include "varinfo.ih"

void VarInfo::writeSheetInfo(ostream &out) const
{
    for (size_t idx = 0, end = d_sheetInfo.size() - 1; idx != end; ++idx)
        out << ' ' << get<0>(d_sheetInfo[idx]) << ':' << 
                    (get<1>(d_sheetInfo[idx]) + 1) << ','; 

    out << ' ' << get<0>(d_sheetInfo.back()) << ':' << 
                  (get<1>(d_sheetInfo.back()) + 1);
}
