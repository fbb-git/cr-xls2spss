#include "varinfo.ih"

VarInfo::VarInfo(string const &varName, 
                size_t lineNr, int type, UUTupleVector &ss,
                AssociationMap &am)
:
    d_varName(varName),
    d_lineNr(lineNr),
    d_type(type),
    d_sheetInfo(move(ss)),
    d_am(move(am))
{}

