#include "varinfo.ih"

unsigned VarInfo::xlsIdx(unsigned sheetNr) const
{
    auto iter = 
        find_if(d_sheetInfo.begin(), d_sheetInfo.end(), 
            [&](UUTuple const &uuTuple)
            {
                return get<0>(uuTuple) == sheetNr;
            }
        );

    return iter == d_sheetInfo.end() ? ~0U : get<1>(*iter);
}
