#include <iostream>
#include <sstream>
#include <string>

#include <bobcat/datetime>

using namespace std;
using namespace FBB;

// https://www.ibm.com/support/knowledgecenter/en/SSLVMB_23.0.0/spss/base/
//                                      syn_get_data_variable_format_txt.html


//    spssTime = excelTime * 24 * 3600 - excelBase + spssBase;

// std          epoch           excel       spss
// 04.09.17     1504483200      42982       13723862400

// 21.12.18     43455
//                                      spss.csv: 13723862400

// 14.12.18     43448       13724640000
// 10.01.19                 13711766400

//  excel base:     std base:       spss base
//  30 dec 1899     1 jan 1970      14 oct. 1582
// -2209161600      1545350400      -12219379200

// e.g. SPSS date value 13510000000
//          13510000000 - 12219379200 = 1290620800

//  From excel numeric time (e.g. 43455) to time since the epoch to standard
//  dd/mm/yy time representation:
//      43455 * 24 * 3600 = 3754512000
//      3754512000 - 2209161600 = 1545350400

//  which can be converted back to dd/mm/yy by DateTime
//  
//  When preferring time since the epoch: 
//      from dd/mm/yy to time: use the std time in seconds the epoch,
//      from excel number since excel's base: number * 24 * 3600 - 2209161600
//  then divide these numbers by 24 * 3600 to obtain the #days.

int main(int argc, char **argv)
{
    cout << argv[1] << '\n';

    time_t excelBase = 2209161600;      // difference with epoch time
    time_t spssBase = 12219379200;      // difference with epoch time

    time_t excel;
    time_t spss;
    time_t sec;

    DateTime dt;

    if (string{ argv[1] }.find('.') == string::npos)
    {
        sec = stoull(argv[1]);
        if (sec > spssBase)
        {
            spss = sec;
            sec = spss - spssBase;
            excel = (sec + excelBase) / (24 * 3600);
        }
        else
        {    
            excel = sec;
            sec = excel * 24 * 3600 - excelBase;
            spss = sec + spssBase;
        }
        dt.setUTCseconds(sec);
    }
    else
    {   
        dt.setHours(0);
        dt.setMinutes(0);
        dt.setSeconds(0);

        istringstream is(argv[1]);
        char dot;
        unsigned day;
        unsigned mon;
        unsigned year;

        if (not (is >> day >> dot >> mon >> dot >> year))
        {
            year = mon % 100;
            mon /= 100;
        }
                
        dt.setYear(2000 + year);
        dt.setMonth(mon - 1);
        dt.setDay(day);

        sec = dt.utcSeconds();

        excel = (sec + excelBase) / (24 * 3600);
        spss = sec + spssBase;
    }
        
    cout << dt << '\n' <<
            excel << ' ' << spss << '\n';

//    dt.setYear(1899);
//    dt.setMonth(11);
//    dt.setDay(30);
//
//
//    dt.setYear(2018);
//    dt.setMonth(11);
//    dt.setDay(21);
//
//    dt.setYear(1582);
//    dt.setMonth(9);
//    dt.setDay(14);
//
//
//    dt.setUTCseconds(13723862400);
//    dt.setUTCseconds(1290620800);               // spss nov. 24 2010
//    dt.setUTCseconds(
//            13764729600
//          - 12219379200
//    );

//    cout << dt << '\n';
//    
//    time_t secs = dt.utcSeconds();
//
//    time_t excelTime = (secs + excelBase) / (24 * 3600);
////  cout << secs << ' ' << (secs / 3600) << ' ' << excelTime << ' ' <<
////                                                 spssTime << '\n';
//
//    excelTime = 42982;
//    excelTime = 42999;
//
//    secs = excelTime * 24 * 3600 - excelBase;
//
//
//    time_t spssTime =  secs + spssBase;
//
//    cout << secs << ' ' << excelTime << ' ' << spssTime << '\n';
}

    
    





