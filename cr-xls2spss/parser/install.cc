#include "parser.ih"

// see README.install

void Parser::install(SNTuple const &spssVar, char type, 
                     UUTupleVector &ss,
                     AssociationMap &am)
{
    if (type != 'n' and am.size() != 0)
        semantic("label maps can only be used with type `n'");
    
    auto const &[name, idx] = spssVar;      // spss variable's name and index

    if (type == 'i')
    {
        if (not get<0>(d_id).empty())
            semanticPrev(": type ID can only be specified once");

        if (ss.size() != d_sheetInfo.size())    
            semanticPrev(
                " ID must be specified for all sheets");

        d_id = spssVar;                 // define the name of the ID var.
    }

    d_xls2spss.add(idx, name, ss);
                                            // store the spec: spssVar ->
                                            // lineNr in the specs file,
                                            // type of the variable (dinsv),
    auto ret = d_varInfoMap.insert(         // its sourceSpec
                    { idx, { name, d_scanner.lineNr() - 1, type, ss, am } } 
                );

    if (
        auto iter =                         // find the XLS variable
        find_if(ss.begin(), ss.end(),       // matching the spss var.     
            [&](UUTuple const &uuTuple)     // in the current sheet
            {
                return get<0>(uuTuple) == d_sheetNr;
            }
        );
        iter != ss.end()
    )
        d_spss2xls[idx] = get<1>(*iter);    // store the spss->xls association

    if (ret.second == false)                        // already present
    {
        ostringstream msg;
        msg << "SPSS variable " << name << " already defined at line " <<
                ret.first->second.specsLineNr();
        semanticPrev(msg.str());
    }
}
