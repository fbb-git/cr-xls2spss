#include "parser.ih"

void Parser::semanticPrev(string const &msg)
{
    cout << "Line " << (d_scanner.lineNr() - 1) << ' ' << 
            d_specsName << ": " << msg << '\n';

    ++d_nErrors_;
}
