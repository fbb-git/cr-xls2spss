#include "parser.ih"

Parser::SNTuple const &Parser::id() const
{
    if (get<0>(d_id).empty())
        throw Exception{} << "SPSS file `" << d_arg[1] << 
                                            "' lacks ID variable";

    return d_id;
}
