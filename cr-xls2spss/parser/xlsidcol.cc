#include "parser.ih"

unsigned Parser::xlsIDidx() const
{
        // ID variables are defined in all sheets, so no
        // test is required whether find succeeds.
    return d_varInfoMap.find(get<1>(d_id))->second.xlsIdx(d_sheetNr);
}
