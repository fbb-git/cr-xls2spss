#include "parser.ih"

string Parser::nVar(unsigned spssVarNr, string label)
{
    VarInfo &varInfo = d_varInfoMap[spssVarNr];

    label = String::trim(label);

    auto iter = varInfo.findLabel(label);

    if (iter != varInfo.end())
        return iter->second;

    auto &labelMap = varInfo.labelMap();

    if (not labelMap.empty())
    {
        cout << "SPSS variable: " << varInfo.name() << " (" << 
                spssVarNr << "): known labels:\n";

        for (auto &labelInfo: labelMap)
            cout << "   " << labelInfo.first << ": " << 
                                                    labelInfo.second << '\n';
    }

    cout << "Specify label value for `" << label << "': ";
    string value;
    getline(cin, value);

    varInfo.addLabel(label, value);

    return value;
}
