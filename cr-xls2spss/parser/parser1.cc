#include "parser.ih"

//  0       1       2       3
// specs spss.csv xls.csv sheetNr

Parser::Parser()
:
    d_arg(Arg::instance()),
    d_specsName(d_arg[0]),
    d_scanner(d_specsName, "-"),
    d_msg(s_msg),
    d_sheetNr(stoul(d_arg[3]))
{
    string tmp;
    d_type = d_arg.option(&tmp, 't') ? tmp[0] : s_type;

    if ("dinsv"s.find(d_type) == string::npos)
        throw Exception{} << "--type must specify one of d, i, n, s, or v, "
                            "not `" << tmp << '\'';
}

