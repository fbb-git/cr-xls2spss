#include "parser.ih"

void Parser::sheetSpec(unsigned sheetNr, unsigned hdrNr, unsigned dataNr)
{
    auto ret = d_sheetInfo.insert({ sheetNr, { hdrNr - 1, dataNr - 1 } });


    if (ret.second == false)
    {
        auto const &tup = ret.first->second;

        cout << "[Warning] Steet " << sheetNr << " already specified with "
                                        "hdr " << (get<0>(tup) + 1) << ", "
                                        "data: " << (get<1>(tup) + 1) << "\n"
                "          ignoring hdr " << hdrNr << ", data: " << 
                                        dataNr << '\n';
    }
    else if (dataNr <= hdrNr)
    {
        ostringstream msg;
        msg << "sheet " << sheetNr << ": headerNr (" << hdrNr << 
                    ") must be smaller than dataNr (" << dataNr << ')';
        semantic(msg.str());
    }
}






