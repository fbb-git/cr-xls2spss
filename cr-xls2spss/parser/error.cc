#include "parser.ih"

void Parser::error()
{
    string const &matched = d_scanner.matched();

    cout << "Line " << d_scanner.lineNr() << ' ' << d_specsName <<
            " at `" <<
            (matched == "\n" ? "EOLN" : matched) << "': " << 
            d_msg << " expected\n";
}
