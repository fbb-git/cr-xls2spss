#include "parser.ih"

//  0       1       2       3
// specs spss.csv xls.csv sheetNr

tuple<unsigned, unsigned> Parser::sheetInfo() const
{
    unsigned sheetNr = d_sheetNr;

    if (d_sheetInfo.find(sheetNr) == d_sheetInfo.end())
        sheetNr = 0;

    return d_sheetInfo.find(sheetNr)->second;
}
