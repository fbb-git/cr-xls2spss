#include "parser.ih"

int Parser::spssVarType(unsigned colIdx) const
{
    auto iter = d_varInfoMap.find(colIdx);

    return iter == d_varInfoMap.end() ? 
                0 
            : 
                d_varInfoMap.find(colIdx)->second.type();
}
