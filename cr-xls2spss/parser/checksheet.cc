#include "parser.ih"

void Parser::checkSheet()
{
    if (d_sheetInfo.find(d_sheetNr) == d_sheetInfo.end())
        semanticPrev("requested sheet (" + 
                            to_string(d_sheetNr) + ") not defined");
}
