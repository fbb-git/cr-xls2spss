#include "parser.ih"

unsigned Parser::positiveNr(std::string const &msg)
{
    unsigned repetition = stoul(d_scanner.matched());

    if (repetition == 0)
        semantic(msg);

    return repetition;
}
