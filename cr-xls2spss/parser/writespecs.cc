#include "parser.ih"

void Parser::writeSpecs() const
{
    auto out{ Exception::factory<ofstream>(d_specsName + ".new") };

    for (auto &sheet: d_sheetInfo)
    {
        if (sheet.first != 0)
            out << "sheet " << sheet.first << 
                    ": hdr " << (get<0>(sheet.second) + 1) << ", "
                    "data " <<  (get<1>(sheet.second) + 1) << '\n';
    }

    out << '\n';

    for (auto &varInfoEl: d_varInfoMap)
    {
                                                // spss var.idx
        out << varInfoEl.second.name() << ' ' << (varInfoEl.first + 1) << ' ';
        varInfoEl.second.write(out);
        out << '\n';
    }
}
