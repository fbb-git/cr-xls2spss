#include "parser.ih"

void Parser::onlyThisSheet()
{
    for (auto &varInfo: d_varInfo)
        varInfo.second.select(d_sheetNr);
}
