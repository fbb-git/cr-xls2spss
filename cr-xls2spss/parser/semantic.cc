#include "parser.ih"

void Parser::semantic(string const &msg)
{
    cout << "Line " << d_scanner.lineNr() << ' ' << d_specsName << ": " << 
            msg << '\n';

    ++d_nErrors_;
}
