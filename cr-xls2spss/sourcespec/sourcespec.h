#ifndef INCLUDED_SOURCESPEC_
#define INCLUDED_SOURCESPEC_

#include <ostream>
#include <string>
#include <vector>
#include <tuple>
#include <unordered_map>

#include "../types/types.h"

class SourceSpec: protected Types
{
    UUTupleVector d_sheetInfo;

    public:
        SourceSpec() = default;
    
             //   sheet, variable number
        SourceSpec(UUTupleVector &sheetInfo);                   // 1.cc

//        void select(unsigned sheetNr);          // only keep the info
                                                // for sheet `sheetNr'


//        occurrenceNr() const;

        void write(std::ostream &out) const;
};

#endif

