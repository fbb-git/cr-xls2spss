#include "sourcespec.ih"

void SourceSpec::write(ostream &out) const
{
    if (unsigned size = d_sheetInfoOrg.size(); size != 0)
    {
        --size;
        out << ' ';

        for (unsigned idx = 0; idx != size; ++idx)
            out << get<0>(d_sheetInfoOrg[idx]) << ':' << 
                        get<1>(d_sheetInfoOrg[idx]) << ", "; 

        out << get<0>(d_sheetInfoOrg.back()) << ':' << 
                get<1>(d_sheetInfoOrg.back()) << ' ';
    }
}
