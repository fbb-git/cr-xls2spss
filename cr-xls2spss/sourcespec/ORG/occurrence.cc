#include "sourcespec.ih"

unsigned SourceSpec::occurrenceNr() const
{
    return d_sheetInfo.empty() ? 0 : get<1>(d_sheetInfo.front());
}
