#include "sourcespec.ih"

SourceSpec::SourceSpec(
    std::string &sourceVar, 
    std::vector<std::tuple<unsigned, unsigned>> &sheetInfo )
:
    d_sourceVar(move(sourceVar)), 
    d_sheetInfo(move(sheetInfo)),
    d_sheetInfoOrg(d_sheetInfo)
{}
