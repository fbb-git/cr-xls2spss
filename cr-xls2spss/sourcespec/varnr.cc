#include "sourcespec.ih"

unsigned SourceSpec::varNr(unsigned sheetNr) const
{
    auto iter = 
        find_if(d_sheetInfo.begin(), d_sheetInfo.end(), 
            [&](UUTuple const &uuTuple)
            {
                return get<0>(uuTuple) == sheetNr;
            }
        );

    return iter == d_sheetInfo.end() ? 0 : get<1>(*iter);
}
