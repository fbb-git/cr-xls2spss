
VarInfo:
    void select(unsigned sheetNr)   - select the (sheet, occurrence) info of 
                                      sheet `sheetNr'
    unsigned occurrenceNr();        - the XLS occurrence number of the
                                      XLS var. matching the SPSS variable.
                                      (0 if not available)
    int setColIdx(unsigned colIdx)  - set the column idx of the spss variable,
                                      return its type
    string xlsVarName()             - the name of the xls variable
    unsigned spssColIdx()           - the col. idx of the spss var.
    unsigned specsLineNr()          - line nr in the specs file
    int type()                      - dinsv-type of the variable
    char const *SPSStype()          - spss-type of the spss variable   

Parser:
        struct XLSInfo
        {
            std::string const &xlsVar;
            unsigned idx = 0;           // Index in the SourceSpc d_sheetInfo
        };                              // vector

    std::string     idName()        - name of the ID variable
    UUTuple         sheetInfo()     - hdr line offset and 1st data line offset
                                      in CSV data. By default (0, 1)
    StrVarInfoMap   varInfo()       - SPSS variable -> VarInfo map
    int             setVarCol(      - stores the spss var.'s col, returns its
                        string spssVar,                                 type
                        unsigned colIdx) 
    void            onlyThisSheet() - keep the VarInfo of this sheet
    XLSInfo         xlsInfo(        - XLS info of the spss var.: XLS name and
                        string spssVar)                          var. idx

XLSData:
    StrUMap d_lineID                - map:  ID value -> Data line idx 
    StrLineIdxVectorMap d_var2col;  - map:  XLS var -> vector of column idices

    -------------------------------------------------------------------------
    bool find(                      - true: xlsIdx: index in the xls data row
            unsigned *xlsIdx, 
            string spssVar)
    DataMatrix data()               - the CSV data matrix
    unsigned idIdx()                - col. idx of the ID var.
    unsigned firstRowIdx()          - idx of the first data() line to use
    unsigned data0nr()              - nr of first CSV data line
    
SPSSData:
    StrUMap d_lineID                - map:  ID value -> d_data line idx 
    USet d_dVars                    - set: indices of d-variables
    USet d_sVars                    - set: indices of s-variables
    --------------------------------------------------------------------------
    void merge(XLSData xlsData)     - merge the XLS data into the SPSS data
    bool errors()                   - true: merging errors encountered
    void writeCSV() const;          - write the new SPSS csv

