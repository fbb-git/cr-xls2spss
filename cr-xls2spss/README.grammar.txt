The specs file contains variable and sheet specifications, one specification
per line. Empty lines are OK, and information on a line starting with // is
ignored.

The specs file start with sheet specifications. Examples:

    sheet 1: hdr 3, data 5
    sheet 2: hdr 2, data 4
    sheet 3: hdr 2, data 4
    sheet 4: hdr 3, data 5
    sheet 5: hdr 3, data 5
    sheet 6: hdr 2, data 4
    sheet 7: hdr 3, data 5

For sheet 1 this means that the header line (holding the variable names) is
the sheet's 3rd line, and the first data line is the sheet's 5th line. The
example is real in the sense that the current xls sheets are organized as
specified. If not specified the first line is assumed to be the header line,
and the csv's 2nd line is assumed to be the first data line.

Following the sheet specifications variables are specified. Variable
specifications start with the SPSS variable name followed by its number in the
spss csv file. Immediately following the spss variable number the variable's
type is specified, which is a single letter d, i, n, s or v.

These types specify:
    d   -   a date variable
    i   -   the subject identification variable
    n   -   a number using a label-to-number map
    s   -   a string variable
    v   -   a (standard) numeric value variable.

A numeric value variable uses integral values like 123, or real values like
-0.23145.

One variable must have type i (the subject-id variable used to identify
subject data. E.g.,

    ID  2  i

meaning that the variable named `ID' is the subject-id variable. This variable
must be present in all sheets (which is indeed the case). Furthermore, since
no additional specification is provided the program assumes that the matching
XLS variable is also named ID.

String (text) variables must explicitly be specified using the s type
specifier. For SPSS text variables are surrounded by double quotes. Examples:

    Complete    3   s
    Complete2   4   s
    Diagnosis2  5   s


Following the type the matching XLS variables are specified using 
comma-separated list of <sheetnr>:<variablenr> specifications. If a variable
should be checked in multiple sheets then multiple specifications are
required, but there should be at least one specification. E.g.,

    ID  2  i    1:3, 2:3, 3:5, 4:3, 5:5, 6:2, 7:2

For ID all sheets must be specified as the ID variable is used to identify
subjects. Otherwise, if multiple sheets are specified identical values must be
encountered for the same subjects. 

XLS string variables that must be converted to numeric values are indicated as
n variables. E.g., the SPSS variable Diagnosis1 is such a variable:

    Diagnosis1 n

The matching XLS variables may already have been partially converted to
numeric variables. If so, the numeric values of the labels and their textual
labels in the XLS file follow the n-line(s), all surrounded by a pair of
square brackets. The numeric values and the labels are separated from each
other by colons, and the transformations separated from each other by
comma's. E.g.,

    Diagnosis1 n 1:5 [ 1: label1, 2: long label, 3: whatever ]

If an unknown label is encountered the label name is shown, as well as the
highest numeric value used so far, and the user specifies the numeric value to
use for that label. Different labels may have identical numeric values.

Date variables are indicated using type d. When numeric values for date
variables are encountered SPSS converts them to dates. Examples:

    DOB     d   3:8, 5:10

If no correspondence between an SPSS variable name and an XLS variable name is
defined, and and the SPSS variable name does not appear in the XLS sheets then
the SPSS variable is not modified by the cr-xls2spss program. The SPSS
variable is also not modified if the corresponding XLS variable is empty or
contains NA.

If the SPSS variable is empty or contains NA then the non-empty/non NA value
of the corresponding XLS variable is inserted into the SPSS csv data file.

If the values of SPSS variables and their corresponding XLS variables do not
match then by default this is considered an error: no new SPSS data file is
produced and the program's output lists the errors. When the program is run
with the option --warnings (or -w) then a new SPSS csv data file is produced
merging the XLS variables with the SPSS variables whereever possible, and
leaving the SPSS variable values as-is when mismatches were encountered. In
the latter case the program's output lists the mismatches as warnings.






