#ifndef INCLUDED_TYPES_
#define INCLUDED_TYPES_

#include <string>
#include <unordered_map>
#include <tuple>
#include <vector>

class VarInfo;

struct Types
{
    typedef std::tuple<std::string, unsigned>               SNTuple;
    typedef std::tuple<unsigned, unsigned>                  UUTuple;
    typedef std::vector<UUTuple>                            UUTupleVector;
    typedef std::vector<std::string>                        StrVect;
    typedef std::unordered_map<unsigned, UUTuple>           UnsUUTupleMap;
    typedef std::unordered_map<std::string, std::string>    AssociationMap;
    typedef AssociationMap::const_iterator                  AMconstIterator;
    typedef std::unordered_map<std::string, unsigned>       StrUMap;
    typedef std::unordered_map<unsigned, VarInfo>           UnsVarInfoMap;
    typedef std::unordered_map<unsigned, unsigned>          UUMap;
    typedef UUMap::value_type                               UUMapValue;
};
        
#endif
